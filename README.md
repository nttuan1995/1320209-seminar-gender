# Gender Classification

## Installtion

1. Install dependencies
  ```

  pip install opencv-contrib-python
  pip install numpy-1.11.3mkl-cp27-cp27m-win32.whl
  pip install scipy-0.19.0-cp27-cp27m-win32.whl
  pip install scikit-learn

  pip install -r requirements.txt
  ```

3. Excute algorithm
  ```
  python main.py
  ```

**Note**: *You must have installed `pip` before.

