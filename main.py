from __future__ import print_function

import os
import numpy as np
import cv2
import logging
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from time import time

# http://scikit-learn.org/stable/auto_examples/applications/face_recognition.html

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def get_faces(path, label):
    person_paths = [os.path.join(path, f) for f in os.listdir(path) if not os.path.isfile(f)]
    faces = []
    labels = []
    row = 140
    col = 140

    for person_path in person_paths:
        images = [os.path.join(person_path, f) for f in os.listdir(person_path) if f.endswith('.jpg')]

        for image in images:
            img = cv2.imread(image)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            face_recs = face_cascade.detectMultiScale(gray, 1.3, 5)
            for (x,y,w,h) in face_recs:
                roi_gray = gray[y:y+h, x:x+w]
                resized_image = cv2.resize(roi_gray, (row, col))
                faces.append(resized_image.flatten())
                labels.append(label)

    return faces, labels

def main():
    male_folder = './data/male'
    female_folder = './data/female'

    # Prepare data
    print("Preparing data from image database")
    t0 = time()
    target_names = np.array(['male', 'female'])
    n_classes = target_names.shape[0]
    male_faces, male_labels = get_faces(male_folder, 0)
    female_faces, female_labels = get_faces(female_folder, 1)
    X = np.array(male_faces + female_faces)
    y = np.array(male_labels + female_labels)
    n_samples = X.shape[0]
    n_features = X.shape[1]

    # split into a training and testing set
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42
    )
    print("done in %0.3fs" % (time() - t0))
    print("Total dataset size:")
    print("n_samples: %d" % n_samples)
    print("n_features: %d" % n_features)
    print("n_classes: %d" % n_classes)

    n_components = 10

    print("Extracting the top %d eigenfaces from %d faces"
      % (n_components, X_train.shape[0]))
    t0 = time()
    pca = PCA(n_components=n_components, svd_solver='randomized',
              whiten=True).fit(X_train)
    print("done in %0.3fs" % (time() - t0))

    print("Projecting the input data on the eigenfaces orthonormal basis")
    t0 = time()
    X_train_pca = pca.transform(X_train)
    X_test_pca = pca.transform(X_test)
    print("done in %0.3fs" % (time() - t0))

    print("Fitting the classifier to the training set")
    t0 = time()
    param_grid = {'C': [1e3, 5e3, 1e4, 5e4, 1e5],
                  'gamma': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1], }
    clf = GridSearchCV(SVC(kernel='rbf', class_weight='balanced'), param_grid)
    clf = clf.fit(X_train_pca, y_train)
    print("done in %0.3fs" % (time() - t0))
    print("Best estimator found by grid search:")
    print(clf.best_estimator_)

    print("Predicting people's gender on the test set")
    t0 = time()
    y_pred = clf.predict(X_test_pca)
    print("done in %0.3fs" % (time() - t0))

    # http://scikit-learn.org/stable/modules/model_evaluation.html#classification-report
    # https://en.wikipedia.org/wiki/Precision_and_recall#Precision
    print(classification_report(y_test, y_pred, target_names=target_names))
    # print(confusion_matrix(y_test, y_pred, labels=range(n_classes)))


if __name__ == "__main__":
    main()
